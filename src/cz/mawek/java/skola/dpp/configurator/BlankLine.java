package cz.mawek.java.skola.dpp.configurator;

import cz.mawek.java.skola.dpp.configurator.parser.TokenType;

/**
 * Token reprezentujuci prazdny riadok.
 * 
 * @author: Marek Gerhart 30.4.2011
 */
public class BlankLine extends Token {

	public BlankLine(String line) {
		this.textLine = line;
	}

	@Override
	public TokenType getType() {
		return TokenType.BLANK_LINE;
	}

}
