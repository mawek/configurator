package cz.mawek.java.skola.dpp.configurator;

import cz.mawek.java.skola.dpp.Tools;
import cz.mawek.java.skola.dpp.configurator.parser.TokenType;

/**
 * Immutable trieda reprezentujuca sekciu. HashCode a Equals musia byt implementovane kvoli tomu ze sekcia sluzi ako kluc v konfiguracii.
 * 
 * @author: Marek Gerhart 30.4.2011
 */
public class Section extends Token {

	// nazov sekcie
	private String name;

	/**
	 * Staticka tovaren na vytvaranie sekcie. Kedze obcas je potrebne vytiahnut z konfiguracie identifikator v sekcii ktoru nemusime mat zrovna k dispozicii, je
	 * potrebne vytvori novu sekciu. Tato metoda sluzi prave na to.
	 * */
	public static Section createSectionForName(String sectionName) {
		return new Section(sectionName, null, null);
	}

	public Section(String sectionName, String sectionComment, String line) {
		this.name = sectionName;
		this.comment = sectionComment;
		this.textLine = line;
	}

	public String getName() {
		return name;
	}

	@Override
	public TokenType getType() {
		return TokenType.SECTION;
	}

	@Override
	public String toString() {
		return name;
	}

	@Override
	public int hashCode() {
		return 37 + name.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof Section)) {
			return false;
		}
		Section section = (Section) obj;

		if (Tools.isSafeStringEqual(this.name, section.name)) {
			return true;
		}

		return false;
	}

}
