package cz.mawek.java.skola.dpp.configurator;

import java.util.List;

import cz.mawek.java.skola.dpp.Pair;

/**
 * Rozhranie reprezentujuce konfiguraciu. Uzivatel pri praci s konfiguraciou obecne nepotrebuje vediet konkretnu implementaciu konfiguracie - potrebuje mat len
 * dostupne hodnoty pre manipulaciu s konfiguraciou.
 * 
 * @author: Marek Gerhart 5.5.2011
 */

public interface Configuration {

	/**
	 * Vlozi vlozi hodnotu do sekcie konfiguracie. Ak sekcia este v konfiguracii neexistuje tak ju zalozi.
	 * 
	 * @param section
	 *            - sekcia do ktorej sa identifikator (parameter) vklada
	 * @param identifier
	 *            - vkladany identifikator (parameter)
	 * */
	public void addParameterValue(Section section, Identifier identifier);

	/**
	 * Prida novu prazdnu sekciu do konfiguracie.
	 * 
	 * @param section
	 *            - vkladana sekcia
	 * */
	public void addSection(Section section);

	/**
	 * Vrati hodnotu parametru sekcie. Typ parametru je specifikovany parametrom. Rovnaku hodnotu je teda mozne vytiahnut v roznych typoch - napr cislo "10" je
	 * mozne vytiahnut ako Integer, ako Double, BigDecimal pripadne ako String a pod.
	 * 
	 * @param sectionName
	 *            - nazov sekcie v ktorej sa identifikator nachadza
	 * @param parameterName
	 *            - identifikator (nazov parametru)
	 * @param clz
	 *            - typ navratovej hodnoty
	 * 
	 * @return hodnota parametru(identifikatora) sekcie
	 * @return null v pripade ze hodnota neexistuje
	 * 
	 * */
	public <T> T getValue(String sectionName, String parameterName, Class<T> clz);

	/**
	 * Vrati hodnotu parametru sekcie. Typ parametru je specifikovany parametrom. Rovnaku hodnotu je teda mozne vytiahnut v roznych typoch - napr cislo "10" je
	 * mozne vytiahnut ako Integer, ako Double, BigDecimal pripadne ako String a pod. V pripade ze sa identifikator v sekcii nenachadza, vrati "fallback"
	 * defaultnu hodnotu predanu v parametry
	 * 
	 * @param sectionName
	 *            - nazov sekcie v ktorej sa identifikator nachadza
	 * @param parameterName
	 *            - identifikator (nazov parametru)
	 * @param clz
	 *            - typ navratovej hodnoty
	 * @param defaultValue
	 *            - defaultna hodnota v pripade ze sa identifikator v sekcii nenachadza
	 * 
	 * @return hodnota parametru sekcie
	 * @return defaultValue v pripade ze sa identifikator v sekcii nenachadza
	 * 
	 * */
	public <T> T getValue(String sectionName, String parameterName, Class<T> clz, T defaultValue);

	/**
	 * Vrati hodnotu parametru sekcie ako zoznam. Konkretny typ ktory sa bude vyskytovat v liste je specifikovany parametrom.
	 * 
	 * @param sectionName
	 *            - nazov sekcie v ktorej sa identifikator nachadza
	 * @param parameterName
	 *            - identifikator (nazov parametru)
	 * @param clz
	 *            - typ navratovej hodnoty
	 * 
	 * @return hodnota parametru sekcie
	 * @return null v pripade ze hodnota neexistuje
	 * 
	 * */
	public <T> List<T> getList(String sectionName, String parameterName, Class<T> clz);

	/**
	 * Vrati hodnotu parametru sekcie ako zoznam. Konkretny typ ktory sa bude vyskytovat v liste je specifikovany parametrom.
	 * 
	 * @param sectionName
	 *            - nazov sekcie v ktorej sa identifikator nachadza
	 * @param parameterName
	 *            - identifikator (nazov parametru)
	 * @param clz
	 *            - typ navratovej hodnoty
	 * @param defaultValue
	 *            - defaultna hodnota v pripade ze sa identifikator v sekcii nenachadza
	 * 
	 * @return hodnota parametru sekcie
	 * @return defaultValue v pripade ze hodnota neexistuje
	 * 
	 * */
	public <T> List<T> getList(String sectionName, String parameterName, Class<T> clz, List<T> defaultValue);

	/**
	 * Zmena hodnoty na referenciu na inu hodnotu.
	 * 
	 * @param sectionName
	 *            - nazov sekcie v ktorej sa nachadza identifikator ktoreho hodnota sa upravuje
	 * @param identifierName
	 *            - nazov upravovaneho identifikatoru
	 * @param reference
	 *            - dvojica <sekcia, identifikator> na ktoru ma vies referencia
	 * */
	public void changeValueToReference(String sectionName, String identifierName, Pair<Section, Identifier> reference);

	/**
	 * Zmeni hodnotu identifikatora v sekcii.
	 * 
	 * @param sectionName
	 *            - nazov sekcie v ktorej sa identifikator nachadza
	 * @param identifierName
	 *            - identifikator(nazov parametru)
	 * @param value
	 *            - nova hodnota identifikatora
	 * */
	public <T> void changeIdentifierValue(String sectionName, String identifierName, T value);

	/**
	 * Zmeni hodnotu identifikatora.
	 * 
	 * * @param identifier - identifikator ktoreho hodnotu cheme zmenit
	 * 
	 * @param value
	 *            - nova hodnota identifikatora
	 * */
	public <T> void changeIdentifierValue(Identifier identifier, T value);

	/**
	 * Zapise konfiguraciu do suboru
	 * */
	public void writeToFile(String fileName);
}
