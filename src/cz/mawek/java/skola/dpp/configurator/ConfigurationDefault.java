package cz.mawek.java.skola.dpp.configurator;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import cz.mawek.java.skola.dpp.Pair;
import cz.mawek.java.skola.dpp.configurator.parser.error.IOUncheckedException;
import cz.mawek.java.skola.dpp.configurator.parser.error.IdentifierNotFoundException;
import cz.mawek.java.skola.dpp.configurator.parser.error.SectionNotFoundException;

/**
 * @author: Marek Gerhart 30.4.2011
 */

public class ConfigurationDefault implements Configuration {

	/*
	 * Mapa v ktorej je ulozena samotna konfiguracia. Klucom do mapy je sekcia, hodnotou je dalsia mapa v ktore su ulozene identifikatory pod ich nazvami.
	 * Konfiguracia je implementovana je ako LinkedHashMap aby sa zachovalo poradie sekcie tak ako idu v subore.
	 */
	protected Map<Section, Map<String, Identifier>> configurationMap = new LinkedHashMap<Section, Map<String, Identifier>>();

	/**
	 * Vlozi vlozi hodnotu do sekcie konfiguracie. Ak sekcia este v konfiguracii neexistuje tak ju zalozi.
	 * 
	 * @param section
	 *            - sekcia do ktorej sa identifikator (parameter) vklada
	 * @param identifier
	 *            - vkladany identifikator (parameter)
	 * */
	@Override
	public void addParameterValue(Section section, Identifier identifier) {
		Map<String, Identifier> identifierMap = configurationMap.get(section);

		if (identifierMap == null) {
			identifierMap = new LinkedHashMap<String, Identifier>();
		}
		identifierMap.put(identifier.getIdentifierName(), identifier);
		configurationMap.put(section, identifierMap);

	}

	/**
	 * Prida novu prazdnu sekciu do konfiguracie.
	 * 
	 * @param section
	 *            - vkladana sekcia
	 * */
	@Override
	public void addSection(Section section) {
		configurationMap.put(section, new LinkedHashMap<String, Identifier>());
	}

	/**
	 * Vrati hodnotu parametru sekcie. Typ parametru je specifikovany parametrom. Rovnaku hodnotu je teda mozne vytiahnut v roznych typoch - napr cislo "10" je
	 * mozne vytiahnut ako Integer, ako Double, BigDecimal pripadne ako String a pod.
	 * 
	 * @param sectionName
	 *            - nazov sekcie v ktorej sa identifikator nachadza
	 * @param parameterName
	 *            - identifikator (nazov parametru)
	 * @param clz
	 *            - typ navratovej hodnoty
	 * 
	 * @return hodnota parametru sekcie
	 * @return null v pripade ze hodnota neexistuje
	 * 
	 * */
	@Override
	public <T> T getValue(String sectionName, String parameterName, Class<T> clz) {
		return getValue(Section.createSectionForName(sectionName), parameterName, clz);
	}

	private <T> T getValue(Section section, String parameterName, Class<T> clz) {
		return getValue(section, parameterName, clz, null);

	}

	/**
	 * Vrati hodnotu parametru sekcie. Typ parametru je specifikovany parametrom. Rovnaku hodnotu je teda mozne vytiahnut v roznych typoch - napr cislo "10" je
	 * mozne vytiahnut ako Integer, ako Double, BigDecimal pripadne ako String a pod. V pripade ze sa identifikator v sekcii nenachadza, vrati "fallback"
	 * defaultnu hodnotu predanu v parametry
	 * 
	 * @param sectionName
	 *            - nazov sekcie v ktorej sa identifikator nachadza
	 * @param parameterName
	 *            - identifikator (nazov parametru)
	 * @param clz
	 *            - typ navratovej hodnoty
	 * @param defaultValue
	 *            - defaultna hodnota v pripade ze sa identifikator v sekcii nenachadza
	 * 
	 * @return hodnota parametru sekcie
	 * @return defaultValue v pripade ze sa identifikator v sekcii nenachadza
	 * 
	 * */
	@Override
	public <T> T getValue(String sectionName, String parameterName, Class<T> clz, T defaultValue) {
		return getValue(Section.createSectionForName(sectionName), parameterName, clz, defaultValue);
	}

	private <T> T getValue(Section section, String parameterName, Class<T> clz, T defaultValue) {
		Map<String, Identifier> identifierMap = configurationMap.get(section);

		if (identifierMap == null) {
			return defaultValue;
		}

		Identifier identifier = identifierMap.get(parameterName);
		if (identifier == null) {
			return defaultValue;
		}

		return identifier.getValue(clz, this);

	}

	/**
	 * Vrati hodnotu parametru sekcie ako zoznam. Konkretny typ ktory sa bude vyskytovat v liste je specifikovany parametrom.
	 * 
	 * @param sectionName
	 *            - nazov sekcie v ktorej sa identifikator nachadza
	 * @param parameterName
	 *            - identifikator (nazov parametru)
	 * @param clz
	 *            - typ navratovej hodnoty
	 * @param defaultValue
	 *            - defaultna hodnota v pripade ze sa identifikator v sekcii nenachadza
	 * 
	 * @return hodnota parametru sekcie
	 * @return defaultValue v pripade ze hodnota neexistuje
	 * 
	 * */
	@Override
	public <T> List<T> getList(String sectionName, String parameterName, Class<T> clz, List<T> defaultValue) {
		return getList(Section.createSectionForName(sectionName), parameterName, clz, defaultValue);
	}

	private <T> List<T> getList(Section section, String parameterName, Class<T> clz, List<T> defaultValue) {
		Map<String, Identifier> identifierMap = configurationMap.get(section);

		if (identifierMap == null) {
			return defaultValue;
		}

		Identifier identifier = identifierMap.get(parameterName);
		if (identifier == null) {
			return defaultValue;
		}

		return identifier.getValueList(clz, this);
	}

	/**
	 * Vrati hodnotu parametru sekcie ako zoznam. Konkretny typ ktory sa bude vyskytovat v liste je specifikovany parametrom.
	 * 
	 * @param sectionName
	 *            - nazov sekcie v ktorej sa identifikator nachadza
	 * @param parameterName
	 *            - identifikator (nazov parametru)
	 * @param clz
	 *            - typ navratovej hodnoty
	 * 
	 * @return hodnota parametru sekcie
	 * @return null v pripade ze hodnota neexistuje
	 * 
	 * */
	@Override
	public <T> List<T> getList(String sectionName, String parameterName, Class<T> clz) {
		return getList(Section.createSectionForName(sectionName), parameterName, clz);
	}

	private <T> List<T> getList(Section section, String parameterName, Class<T> clz) {
		return getList(section, parameterName, clz, null);
	}

	/**
	 * Zmena hodnoty na referenciu na inu hodnotu. Zatial neimplementovane. Mozno vo verzii 2 :)
	 * */
	@Override
	public void changeValueToReference(String sectionName, String identifierName, Pair<Section, Identifier> reference) {
		// TODO: implement me
		throw new UnsupportedOperationException("Not implemented!");
	}

	/**
	 * Zmeni hodnotu identifikatora v sekcii.
	 * 
	 * @param section
	 *            - sekcia v ktorej sa identifikator nachadza
	 * @param identifierName
	 *            - identifikator(nazov parametru)
	 * @param value
	 *            - nova hodnota identifikatora
	 * @throws SectionNotFoundException
	 *             - v pripade ze sa nenajde specifikovana sekcia
	 * @throws IdentifierNotFoundException
	 *             - v pripade ze sa nenajde identifikator so zadanym nazvom
	 * */
	@Override
	public <T> void changeIdentifierValue(String sectionName, String identifierName, T value) throws SectionNotFoundException, IdentifierNotFoundException {
		changeIdentifierValue(Section.createSectionForName(sectionName), identifierName, value);
	}

	private <T> void changeIdentifierValue(Section section, String identifierName, T value) throws SectionNotFoundException, IdentifierNotFoundException {
		Map<String, Identifier> identifierMap = configurationMap.get(section);

		if (identifierMap == null) {
			throw new SectionNotFoundException("Section with name[" + section.getName() + "] not found!");
		}

		Identifier identifier = identifierMap.get(identifierName);
		if (identifier == null) {
			throw new IdentifierNotFoundException("Identifier with name[" + identifierName + "] in section[" + section.getName() + "] not found!");
		}

		identifier.<T> changeValue(value);

	}

	/**
	 * Zmeni hodnotu identifikatora.
	 * 
	 * * @param identifier - identifikator ktoreho hodnotu cheme zmenit
	 * 
	 * @param value
	 *            - nova hodnota identifikatora
	 * */
	@Override
	public <T> void changeIdentifierValue(Identifier identifier, T value) {
		identifier.<T> changeValue(value);
	}

	/**
	 * Zapise konfiguraciu do suboru.
	 * 
	 * @param fileName
	 *            - nazov suboru kde sa ma zapisat konfiguracia
	 * */
	@Override
	public void writeToFile(String fileName) {
		BufferedWriter out = null;

		// Hlbka zanorenia rastie kvoli kvoli try/catch - co uz..
		try {
			out = new BufferedWriter(new FileWriter(fileName));

			for (Section section : configurationMap.keySet()) {
				out.write(section.getTextRepresentation());
				out.newLine();

				for (Identifier identifier : configurationMap.get(section).values()) {
					out.write(identifier.getTextRepresentation());
					out.newLine();
				}
				out.newLine();
			}

		} catch (IOException e) {
			throw new IOUncheckedException("Error during writing configuration to file:[" + fileName + "].", e);
		} finally {
			if (out != null) {
				try {
					out.close();
				} catch (IOException e) {
					/* ignore */
				}
			}
		}

	}
}
