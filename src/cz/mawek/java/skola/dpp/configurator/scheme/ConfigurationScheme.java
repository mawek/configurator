package cz.mawek.java.skola.dpp.configurator.scheme;

/**
 * @author: Marek Gerhart 5.5.2011
 */

public interface ConfigurationScheme {

	public void addIdentifierSchemeValue(SectionScheme sectionScheme, IdentifierScheme identifierScheme);

	public void addSectionScheme(SectionScheme sectionScheme);

	public IdentifierScheme getIdentifierScheme(SectionScheme sectionScheme, String identifierName);

}
