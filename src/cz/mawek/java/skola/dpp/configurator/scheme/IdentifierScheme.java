package cz.mawek.java.skola.dpp.configurator.scheme;

/**
 * @author: Marek Gerhart 5.5.2011
 */

public class IdentifierScheme extends SchemeToken {

	private String name;
	private boolean listValues;
	private Class<?> valueType;
	private String defaultValue;

	public IdentifierScheme(String name, boolean mandatory, boolean listValues, Class<?> valueType, String comment, String defaultValue) {

	}

	public String getName() {
		return name;
	}

	public boolean isListValues() {
		return listValues;
	}

	public Class<?> getValueType() {
		return valueType;
	}

	public String getDefaultValue() {
		return defaultValue;
	}
}
