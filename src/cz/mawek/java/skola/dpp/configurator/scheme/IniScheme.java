package cz.mawek.java.skola.dpp.configurator.scheme;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author: Marek Gerhart 5.5.2011
 */

public class IniScheme {

	protected Map<SectionScheme, Map<String, IdentifierScheme>> configurationMap = new LinkedHashMap<SectionScheme, Map<String, IdentifierScheme>>();
}
