package cz.mawek.java.skola.dpp.configurator.scheme;

/**
 * @author: Marek Gerhart 5.5.2011
 */

public abstract class SchemeToken {

	protected boolean mandatory;
	protected String comment;

	public String getComment() {
		return comment;
	}

	public boolean isMandatory() {
		return mandatory;
	}
}
