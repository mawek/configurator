package cz.mawek.java.skola.dpp.configurator.scheme;

import cz.mawek.java.skola.dpp.Tools;

/**
 * @author: Marek Gerhart 5.5.2011
 */

public class SectionScheme extends SchemeToken {

	private String name;

	public SectionScheme(String name, boolean mandatory, String comment) {
		this.name = name;
		this.mandatory = mandatory;
		this.comment = comment;
	}

	public String getName() {
		return name;
	}

	@Override
	public String toString() {
		return name;
	}

	@Override
	public int hashCode() {
		return 37 + name.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof SectionScheme)) {
			return false;
		}
		SectionScheme section = (SectionScheme) obj;

		if (Tools.isSafeStringEqual(this.name, section.name)) {
			return true;
		}

		return false;
	}

}
