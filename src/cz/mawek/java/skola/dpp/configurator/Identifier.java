package cz.mawek.java.skola.dpp.configurator;

import java.util.LinkedList;
import java.util.List;

import cz.mawek.java.skola.dpp.Pair;
import cz.mawek.java.skola.dpp.Tools;
import cz.mawek.java.skola.dpp.configurator.parser.IdentifierParser;
import cz.mawek.java.skola.dpp.configurator.parser.TokenType;
import cz.mawek.java.skola.dpp.configurator.parser.convert.ValueConformerHelper;
import cz.mawek.java.skola.dpp.configurator.parser.error.ImpossibleConversionException;

/**
 * Token reprezentujuci identifikator.
 * 
 * @author: Marek Gerhart 30.4.2011
 */

public class Identifier extends Token {
	// nazov identifikatora
	private String identifierName;

	// retazcova reprezentacia neinterpretovanej hodnoty identifikatora - hodnota, alebo retazcova reprezentacia referencie/zoznamu tak ako sa precita zo suboru
	private String identifierValue;

	// priznak ci bola hodnota identifikatora zmenena uzivatelom, pouziva sa pri ukladani konfiguracie
	private boolean changed;

	// kesovana hodnota identifikatora, ziskavanie (rozparsovanie/interpretovanie) hodnoty moze byt narocne tak sa hodnota kesuje
	private Object cachedValue;

	public Identifier(String identifierName, String identifierValue, String identifierComment, String line) {
		this.identifierName = identifierName;
		this.identifierValue = identifierValue;
		this.comment = identifierComment;
		this.textLine = line;
	}

	@Override
	public TokenType getType() {
		return TokenType.IDENTIFIER;
	}

	/**
	 * Parametricka metoda, ktora vracia hodnotu identifikatora konfiguracie. Typ navratovej hodnoty je specifikovany v parametry.
	 * 
	 * @return hodnotu identifikatora konfiguracie ak je dostupna a je mozna konverzia na pozadovany typ
	 * 
	 * @throws ImpossibleConversionException
	 *             ak neexistuje convertor ktory vie convertovat hodnotu na pozadovany typ alebo ak convertor existuje ale nevie spravne convertovat hodnotu na
	 *             pozadovany typ - moze nastat napr ak sa pokusame konvertovat hodnotu "abc" na Integer
	 * 
	 * */
	@SuppressWarnings("unchecked")
	public <T> T getValue(Class<T> clz, Configuration configuration) {

		/*
		 * Ak je hodnota kesovana a je rovnakeho typu aky je pozadovany tak sa rovno vrati. Uzivatel moze pozadovat hodnotu v roznych typoch (napr hodnotu "10"
		 * moze pozadovat ako Integer/Doble/String, preto treba vzdy skontrolovat aj spravny typ kesovanej hodnoty.Tiez musim zistovat ci sa nejedna o
		 * referenciu - pretoze identifikator na ktory sa odkazujem sa mohol medzicasom zmenit
		 */
		if (cachedValue != null && clz.isInstance(cachedValue) && !IdentifierParser.isReference(identifierValue)) {
			return (T) cachedValue;
		}

		cachedValue = getValueFromString(identifierValue, clz, configuration);

		return (T) cachedValue;

	}

	private <T> T getValueFromString(String value, Class<T> clz, Configuration configuration) {
		// v pripade, ze sa jedna o hodnotu typu referencia, musim ziskat hodnotu z ineho identifikatora
		if (IdentifierParser.isReference(value)) {
			return (T) getValueFromReference(value, clz, configuration);
		}

		// ak sa jedna o samostatnu hodnotu tak sa ju rovno pokusim skonvertovat cez conformer

		/*
		 * predbezne zakomentovane - aj v pripade ze je hodnota list tak sa pokusim hodnotu skonvertovat na jednorozmernu strukturu, predpokladam ze pouzivatel
		 * vie co robi - konieckoncov vzdy si moze vytiahnut zoznam ako String :if (type == IdentifierValueType.VALUE) {
		 */
		return (T) ValueConformerHelper.convert(clz, value);
	}

	/**
	 * Parametricka metoda, ktora vracia hodnotu identifikatora konfiguracie ako list(zoznam hodnot). Typ hodnot v liste je specifikovany v parametry.
	 * 
	 * @return list hodnot konvertovanych na pozadovany typ
	 * 
	 * @throws ImpossibleConversionException
	 *             ak neexistuje convertor ktory vie convertovat niektoru z poloziek listu na pozadovany typ alebo ak convertor existuje ale nevie spravne
	 *             convertovat hodnotu na pozadovany typ - moze nastat napr ak sa pokusame konvertovat hodnotu "abc" na Integer *
	 * 
	 * */
	public <T> List<T> getValueList(Class<T> clz, ConfigurationDefault configuration) {

		// v pripade ze hodnota nie je zoznam tak vratim zoznam s jednou polozkou
		if (!IdentifierParser.isList(identifierValue)) {
			List<T> resultList = new LinkedList<T>();
			resultList.add(getValue(clz, configuration));
			return resultList;
		}

		return getListFromValue(clz, configuration);
	}

	private <T> List<T> getListFromValue(Class<T> clz, ConfigurationDefault configuration) {

		// ziskam parser ktory vie parsovat identifikatory a rozbijem riadok na zoznam stringov ktore neskor skonvertujem na spravny typ
		IdentifierParser parser = TokenType.getParserForTokenType(TokenType.IDENTIFIER);
		List<String> stringValueList = parser.parseStringListValues(identifierValue);

		// konverzia rozparsovanych stringov na spravny format
		List<T> valueList = new LinkedList<T>();
		for (String listItem : stringValueList) {
			valueList.add(getValueFromString(listItem, clz, configuration));
		}

		return valueList;
	}

	/*
	 * Ziska hodnotu z referencovaneho identifikatora
	 */
	private static <T> T getValueFromReference(String strinValue, Class<T> clz, Configuration configuration) {

		IdentifierParser parser = TokenType.getParserForTokenType(TokenType.IDENTIFIER);

		Pair<Section, String> reference = parser.parseReferencePair(strinValue);

		return configuration.getValue(reference.getParameter().getName(), reference.getValue(), clz);
	}

	/**
	 * Zmeni hodnotu identifikatora.
	 * 
	 * * @param identifier - identifikator ktoreho hodnotu cheme zmenit
	 * 
	 * @param value
	 *            - nova hodnota identifikatora
	 * */
	public <T> void changeValue(T newValue) {
		changed = true; // poznac si ze hodnota sa zmenila, neskor to vyuzijes pri ukladani hodnoty identifikatoru

		cachedValue = newValue;

		identifierValue = newValue == null ? "" : newValue.toString();
	}

	public String getIdentifierName() {
		return identifierName;
	}

	public String getIdentifierValue() {
		return identifierValue;
	}

	public boolean isChanged() {
		return changed;
	}

	/**
	 * Vrati textovu reprezentaciu identifikatoru - tak ako je zapisany v konfiguracnom subore. Vecsinou to bude atribut textLine, ale pri zmene hodnoty
	 * identifikatora sa musi tento retazec pregenerovat.
	 * 
	 * @return textova reprezentacia identifikatora
	 * @return prazny retazec ak sa hodnota identifikatora zmenila na null
	 * */
	@Override
	public String getTextRepresentation() {
		if (changed) {

			// ak je hodnota identifikatora prazdna tak chcem odstranit identifikator z konfiguraku - nema zmysel ho tam zapisovat bez hodnoty
			if (Tools.isEmptyString(identifierValue)) {
				return "";
			}

			StringBuffer strBuf = new StringBuffer();

			strBuf.append(identifierName);
			strBuf.append(" = ");
			strBuf.append(cachedValue.toString()); // cachedValue musi byt nenullova pri zmene hodnoty

			if (!Tools.isEmptyString(comment)) {
				strBuf.append(";");
				strBuf.append(comment);
			}
			textLine = strBuf.toString();
		}

		return textLine;
	}

	@Override
	public String toString() {
		return super.toString() + ", identifier name:[" + identifierName + "], identifier value:[" + identifierValue + "]";
	}

}
