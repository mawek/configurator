package cz.mawek.java.skola.dpp.configurator;

import cz.mawek.java.skola.dpp.configurator.parser.TokenType;

/**
 * Abstraktna trieda, ktora sluzi ako predok pre vsetky tokeny (sekcia, identifikator, ...). Token zvycajne reprezentuje jeden riadok z konfiguracneho suboru.
 * 
 * @author: Marek Gerhart 1.5.2011
 */

public abstract class Token {

	/**
	 * Komentar k tokenu.
	 * */
	protected String comment;

	/**
	 * Retazcova reprezentacia tokenu v konfiguracnom subore. Retazec obsahuje cely riadok, tak ako sa z konfiguracneho suboru precita - vratane komentarov,
	 * identifikatora a hodnot a vsetkych znakov
	 * */
	protected String textLine;

	/**
	 * Metoda, ktoru musia potomkovia implementovat - specifikuje konkretny typ tokenu.
	 * */
	public abstract TokenType getType();

	/**
	 * Parametricka metoda, ktora vracia spravny typ tohto objektu.
	 * */
	@SuppressWarnings("unchecked")
	public <T extends Token> T cast() {
		return (T) this;
	}

	public String getComment() {
		return comment;
	}

	/**
	 * Vrati textovu reprezentaciu tokenu - tak ako je zapisany v konfiguracnom subore. Vecsinou to bude atribut textLine.
	 * */
	public String getTextRepresentation() {
		return textLine;
	}

	@Override
	public String toString() {
		return "Original text:[" + textLine + "], comment:[" + comment + "]";
	}
}
