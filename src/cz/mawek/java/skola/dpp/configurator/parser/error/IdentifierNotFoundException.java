package cz.mawek.java.skola.dpp.configurator.parser.error;

/**
 * @author: Marek Gerhart 3.5.2011
 */

public class IdentifierNotFoundException extends RuntimeException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8760030829385680791L;

	public IdentifierNotFoundException(String message, Throwable cause) {
		super(message, cause);
	}

	public IdentifierNotFoundException(String message) {
		super(message);
	}
}
