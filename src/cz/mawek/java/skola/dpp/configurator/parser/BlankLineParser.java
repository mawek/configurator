package cz.mawek.java.skola.dpp.configurator.parser;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import cz.mawek.java.skola.dpp.configurator.BlankLine;
import cz.mawek.java.skola.dpp.configurator.parser.error.ParserException;

/**
 * @author: Marek Gerhart 3.5.2011
 */

public class BlankLineParser implements TokenParser {

	private static final Pattern BLANK_LINE_PATTERN = Pattern.compile("^(\\s)*$");

	@Override
	public Pattern getTokenPattern() {
		return BLANK_LINE_PATTERN;
	}

	@Override
	public BlankLine parseToken(String line) {
		Matcher matcher = BLANK_LINE_PATTERN.matcher(line);
		if (!matcher.matches()) {
			throw new ParserException("Line '" + line + "' does't match blank line regular expression '" + BLANK_LINE_PATTERN.pattern() + "'");
		}
		return new BlankLine(line);
	}
}
