package cz.mawek.java.skola.dpp.configurator.parser;

import java.util.regex.Pattern;

import cz.mawek.java.skola.dpp.configurator.Token;

/**
 * Rozhranie ktore implementuju vsetky parsery tokenov.
 * 
 * @author: Marek Gerhart 3.5.2011
 */

public interface TokenParser {

	/**
	 * Metoda ktora parsuje riadok na pozadovany token.
	 * */
	public Token parseToken(String line);

	/**
	 * Regularny vyraz ktory sa pouziva pri rozhodovani ci je mozne tento parser pouzit na parsovanie riadku (teda akeho typu - tokenu je dany riadok)
	 * */
	public Pattern getTokenPattern();

}
