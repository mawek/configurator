package cz.mawek.java.skola.dpp.configurator.parser;

import java.io.File;
import java.util.HashSet;
import java.util.Set;

import cz.mawek.java.skola.dpp.configurator.parser.error.NoSuitableParserFoundException;

/**
 * Factory trieda ktora podla nazvu suboru rozozna aky parser je treba pouzit na parsovanie daneho suboru.
 * 
 * @author: Marek Gerhart 30.4.2011
 */

public final class ConfigurationParserFactory {

	private ConfigurationParserFactory() {
	}

	// registrovane parsery
	private static Set<ConfigurationParser> parserSet = new HashSet<ConfigurationParser>();
	static {
		parserSet.add(new IniParser());
	}

	/**
	 * Zaregistrovat novy parser konfiguracie.
	 * 
	 * @param parser
	 *            - novy parser konfiguracie
	 * */
	public static void registerConfigurationParser(ConfigurationParser parser) {
		parserSet.add(parser);
	}

	/**
	 * Odregistrovat parser konfiguracie.
	 * 
	 * @param parser
	 *            - parser ktory chceme odregistrovay z dostupnych parserov
	 * */
	public static void unRegisterConfigurationParser(ConfigurationParser parser) {
		parserSet.remove(parser);
	}

	/**
	 * Metoda ktora vracia spravny parser pre zadany nazov suboru.
	 * 
	 * @throws NoSuitableParserFoundException
	 *             - v pripade ze nie je zaregistrovany ziadny parser vhodny ku spracovaniu suboru s danym nazvom
	 * */
	public static ConfigurationParser getParserForFileName(String fileName) {
		for (ConfigurationParser parser : parserSet) {
			if (parser.canHandle(fileName)) {
				return parser;
			}
		}
		throw new NoSuitableParserFoundException("No suitable parser found for file: " + fileName);
	}

	/**
	 * Metoda ktora vracia spravny parser pre zadany subor.
	 * 
	 * @throws NoSuitableParserFoundException
	 *             - v pripade ze nie je zaregistrovany ziadny parser vhodny ku spracovaniu suboru
	 * */
	public static ConfigurationParser getParserForFile(File file) {
		return getParserForFileName(file.getName());
	}
}
