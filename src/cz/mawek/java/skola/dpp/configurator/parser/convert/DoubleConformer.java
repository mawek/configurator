package cz.mawek.java.skola.dpp.configurator.parser.convert;

import cz.mawek.java.skola.dpp.configurator.parser.error.ImpossibleConversionException;

/**
 * Conformer pre typ Double - sluzi na konverziu retazcovej reprezentacie Double-u.
 * 
 * @author: Marek Gerhart 3.5.2011
 */

public final class DoubleConformer implements Conformer<Double> {

	/**
	 * @return true ak je parameter typu Double
	 * @return false inak
	 * */
	public boolean isAssignable(Class<?> clz) {

		if (clz.isInstance(Double.valueOf(1))) {
			return true;
		}
		return false;
	}

	/**
	 * Vracia Double rozparsovany z parametra.
	 * 
	 * @return - vyparsovany Double
	 * */
	public Double convertFromString(String string) {
		try {
			return Double.valueOf(string);
		} catch (NumberFormatException e) {
			throw new ImpossibleConversionException("Could not convert value\"" + string + "\" to Double.", e);
		}
	}
}
