package cz.mawek.java.skola.dpp.configurator.parser;

/**
 * @author: Marek Gerhart 1.5.2011
 */

public enum TokenType {
	COMMENT(new CommentParser()), SECTION(new SectionParser()), IDENTIFIER(new IdentifierParser()), BLANK_LINE(new BlankLineParser());

	// parser pre dany token
	private final TokenParser tokenParser;

	private TokenType(TokenParser parser) {
		this.tokenParser = parser;
	}

	/**
	 * Vracia parser pre ktory parsuje token tohto typu.
	 * 
	 * */
	public TokenParser getTokenParser() {
		return tokenParser;
	}

	/**
	 * Vracia parser pre dany typ tokenu. V podstate sa jedna len o pretypovanie.
	 * 
	 * @param tokenType
	 * 
	 * @return parser pre dany typ tokenu
	 * */
	@SuppressWarnings("unchecked")
	public static <T extends TokenParser> T getParserForTokenType(TokenType tokenType) {
		return (T) tokenType.getTokenParser();
	}

}
