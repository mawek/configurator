package cz.mawek.java.skola.dpp.configurator.parser;

import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import cz.mawek.java.skola.dpp.Pair;
import cz.mawek.java.skola.dpp.configurator.Identifier;
import cz.mawek.java.skola.dpp.configurator.Section;
import cz.mawek.java.skola.dpp.configurator.parser.error.ParserException;

/**
 * @author: Marek Gerhart 3.5.2011
 */

public class IdentifierParser implements TokenParser {

	static final String IDENTIFIER_REGEXP = "[.:$a-zA-Z][ .:a-zA-Z0-9_~-]*";

	private static final Pattern IDENTIFIER_TOKEN_PATTERN = Pattern.compile("(\\s)*(" + IDENTIFIER_REGEXP + ")=([^;]+)(;(.+))?");

	private static final String ESCAPED_BLANK_VALUE_START = "\\\\\\s.*";

	private static final Pattern IDENTIFIER_REFERENCE_PATTERN = Pattern.compile("^[^\\\\]?[$][{]([^#]+)#(.+)[}]$");

	private static class ListParsingAutomat {

		static boolean isList(String line) {
			boolean isDolar = false;
			boolean isDolarBracket = false;
			boolean isBackSlash = false;

			for (int i = 0; i < line.length(); ++i) {
				char character = line.charAt(i);

				if (character == '\\') {
					if (isBackSlash) {
						isBackSlash = false;
					} else {
						isBackSlash = true;
					}

				} else if (character == '$') {
					isDolar = true;

				} else if (character == '{') {
					if (isDolar) {
						isDolarBracket = true;
					}

				} else if (character == '}') {
					if (isDolarBracket) {
						isDolarBracket = false;
					}

				} else if (character == ':' || character == ',') {
					if (!isDolarBracket && !isBackSlash) {
						return true;
					}
				}
			}

			return false;
		}

		static List<String> parseList(String line) {
			boolean isDolar = false;
			boolean isDolarBracket = false;
			boolean isBackSlash = false;

			int itemIndexStart = 0;
			int itemIndexEnd = 0;
			boolean inCharacter = true;

			List<String> parsedList = new LinkedList<String>();

			for (int i = 0; i < line.length(); ++i) {
				char character = line.charAt(i);

				if (character == '\\') {
					if (isBackSlash) {
						isBackSlash = false;
					} else {
						isBackSlash = true;
					}

				} else if (character == '$') {
					isDolar = true;

				} else if (character == '{') {
					if (isDolar) {
						isDolarBracket = true;
					}

				} else if (character == '}') {
					if (isDolarBracket) {
						isDolarBracket = false;
					}

				} else if (character == ':' || character == ',') {
					if (!isDolarBracket && !isBackSlash) {
						itemIndexEnd = i;
						inCharacter = false;
					} else {
						isBackSlash = false;
					}
				}

				if (!inCharacter) {
					if (itemIndexEnd >= itemIndexStart) {
						parsedList.add(line.substring(itemIndexStart, itemIndexEnd));
						itemIndexStart = i + 1;
						inCharacter = true;
					}
				}
			}

			itemIndexEnd = line.length();
			if (itemIndexEnd >= itemIndexStart) {
				parsedList.add(line.substring(itemIndexStart, itemIndexEnd));
			}

			return parsedList;
		}
	}

	/**
	 * Zo zadaneho retazca vyparsuje zoznam retazcov
	 * */
	public List<String> parseStringListValues(String str) {
		if (!ListParsingAutomat.isList(str)) {
			throw new ParserException("Cannot parser list from string:[" + str + "].");
		}

		return ListParsingAutomat.parseList(str);
	}

	/**
	 * Zo zadaneho retazca vyparsuje dvojicu - sekcia, nazov parametru
	 * */
	public Pair<Section, String> parseReferencePair(String str) {
		Matcher matcher = IDENTIFIER_REFERENCE_PATTERN.matcher(str);
		if (!matcher.matches()) {
			throw new ParserException("Cannot parser referenced section and value from string:[" + str + "].");
		}

		return new Pair<Section, String>(Section.createSectionForName(matcher.group(1)), matcher.group(2));
	}

	@Override
	public Pattern getTokenPattern() {
		return IDENTIFIER_TOKEN_PATTERN;
	}

	/**
	 * Metoda, ktora rozparsuje retazec a vrati reprezentujuci identifikator. Metoda neparsuje hodnotu identifikatora - ta sa zistuje az ked je potrebna. Jeden
	 * z dovodov je ten, ze identifikatory sa na seba mozu navzajom odkazovat a v momente ked sa nacitava jeden identifikator, nemusi byt este odkazovany
	 * identifikator nacitany. Dalsia z vyhod neskorsieho ziskavania hodnoty je ta, ze retazcovu hodnotu je mozne vratit v roznych typoc podla specifikovania
	 * parametra (@see {@link Identifier#getValue(Class, cz.mawek.java.skola.dpp.configurator.Configuration)})
	 * 
	 * @paran line - retazcova reprezentacia identifikatora
	 * @return vyparsovany identifikator
	 * */
	@Override
	public Identifier parseToken(String line) {
		Matcher matcher = IDENTIFIER_TOKEN_PATTERN.matcher(line);
		if (!matcher.matches()) {
			throw new ParserException("Line '" + line + "' does't match identifier regular expression '" + IDENTIFIER_TOKEN_PATTERN.pattern() + "'");
		}

		String identifierName = matcher.group(2);
		identifierName = identifierName == null ? null : identifierName.trim();

		String identifierComment = matcher.group(5);
		identifierComment = identifierComment == null ? null : identifierComment.trim();

		String identifierValue = matcher.group(3);
		if (identifierValue != null) {
			identifierValue = identifierValue.trim();

			if (identifierValue.matches(ESCAPED_BLANK_VALUE_START)) {
				identifierValue = identifierValue.substring(1);
			}
		}

		return new Identifier(identifierName, identifierValue, identifierComment, line);
	}

	public static boolean isReference(String value) {
		return IDENTIFIER_REFERENCE_PATTERN.matcher(value).matches();
	}

	public static boolean isList(String value) {
		return ListParsingAutomat.isList(value);
	}
}
