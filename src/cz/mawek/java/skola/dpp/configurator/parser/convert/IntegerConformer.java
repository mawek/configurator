package cz.mawek.java.skola.dpp.configurator.parser.convert;

import cz.mawek.java.skola.dpp.configurator.parser.error.ImpossibleConversionException;

/**
 * Conformer pre typ Integer - sluzi na konverziu retazcovej reprezentacie Integer.
 * 
 * @author: Marek Gerhart 3.5.2011
 */

public final class IntegerConformer implements Conformer<Integer> {

	/**
	 * @return true ak je parameter typu Integer
	 * @return false inak
	 * */
	public boolean isAssignable(Class<?> clz) {

		// prvych par hodnot sa kesuje takze to je zadarmo
		if (clz.isInstance(Integer.valueOf(1))) {
			return true;
		}
		return false;
	}

	/**
	 * Vracia Integer rozparsovany z parametra.
	 * 
	 * @return - vyparsovany Double
	 * */
	public Integer convertFromString(String string) {

		try {
			if (string.startsWith("0b") && string.length() > 2) {
				return Integer.parseInt(string.substring(2), 2);
			}
			return Integer.valueOf(string);
		} catch (NumberFormatException e) {
			throw new ImpossibleConversionException("Could not convert value\"" + string + "\" to Integer.", e);
		}
	}
}
