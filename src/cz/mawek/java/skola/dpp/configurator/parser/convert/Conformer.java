package cz.mawek.java.skola.dpp.configurator.parser.convert;

import cz.mawek.java.skola.dpp.configurator.parser.error.ImpossibleConversionException;

/**
 * Rozhranie, ktore musia implementovat vsetky triedy, ktore maju sluzit na konverziu retazcovej reprezentacie hodnoty na spravny typ.
 * 
 * @author: Marek Gerhart 3.5.2011
 */

public interface Conformer<T> {

	/**
	 * Metoda, ktora zistuje ci vie conformer urobit potrebnu konverziu. Teda ak chceme spravit konverziu na typ Integer, tak tato metoda vrati true ak tuto
	 * konverziu spravit vie, inak vrati false.
	 * 
	 * @param clz
	 *            - typ, na ktory potrebujeme konverziu spravit
	 * 
	 * @return true ak conformer vie spracovat konverziu
	 * @return false inak
	 * */
	public boolean isAssignable(Class<?> clz);

	/**
	 * Metoda, ktora skonvertuje (rozparsuje) hodnotnu na spravny typ. Napr ak je v retazci hodnota "10.0" a pozadovany typ je integer tak to vrati hodnotu
	 * 
	 * Integer.valueOf(str)
	 * 
	 * @return hodnota retazca so spravnym typom
	 * @throws ImpossibleConversionException
	 *             ak sa nepodari skonvertovat hodnotu na pozadovany typ
	 * */
	public T convertFromString(String string) throws ImpossibleConversionException;
}
