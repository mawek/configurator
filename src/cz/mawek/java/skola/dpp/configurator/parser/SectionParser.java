package cz.mawek.java.skola.dpp.configurator.parser;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import cz.mawek.java.skola.dpp.configurator.Section;
import cz.mawek.java.skola.dpp.configurator.parser.error.ParserException;

/**
 * @author: Marek Gerhart 3.5.2011
 */

public class SectionParser implements TokenParser {

	private static final Pattern SECTION_PATTERN = Pattern.compile("(\\s)*\\[(" + IdentifierParser.IDENTIFIER_REGEXP + ")\\](\\s)*(;(.*))*$");

	@Override
	public Pattern getTokenPattern() {
		return SECTION_PATTERN;
	}

	@Override
	public Section parseToken(String line) {
		Matcher matcher = SECTION_PATTERN.matcher(line);
		if (!matcher.matches()) {
			throw new ParserException("Line '" + line + "' does't match section regular expression '" + SECTION_PATTERN.pattern() + "'");
		}

		String sectionName = matcher.group(2);
		sectionName = sectionName == null ? null : sectionName.trim();

		String sectionComment = matcher.group(4);
		sectionComment = sectionComment == null ? null : sectionComment.trim();

		return new Section(sectionName, sectionComment, line);
	}
}
