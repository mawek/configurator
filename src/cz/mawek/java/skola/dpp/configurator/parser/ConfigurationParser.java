package cz.mawek.java.skola.dpp.configurator.parser;

import cz.mawek.java.skola.dpp.configurator.Configuration;

/**
 * @author: Marek Gerhart 30.4.2011
 */

public interface ConfigurationParser {
	public Configuration readConfiguration(String configurationPath);

	public Configuration reloadConfiguration();

	public void saveConfiguration();

	public boolean canHandle(String fileName);
}
