package cz.mawek.java.skola.dpp.configurator.parser.convert;

import java.util.Arrays;
import java.util.List;

import cz.mawek.java.skola.dpp.configurator.parser.error.ImpossibleConversionException;

/**
 * Conformer pre typ Boolean - sluzi na konverziu retazcovej reprezentacie Booleanu.
 * 
 * @author: Marek Gerhart 3.5.2011
 */

public final class BooleanConformer implements Conformer<Boolean> {

	// mozne false reprezentacie booleanu
	private static final List<String> FALSE_LIST = Arrays.asList(new String[] { "0", "f", "n", "off", "no", "disabled", });

	// mozne true reprezentacie booleanu
	private static final List<String> TRUE_LIST = Arrays.asList(new String[] { "1", "t", "y", "on", "yes", "enabled", });

	/**
	 * @return true ak je parameter typu BigDecimal
	 * @return false inak
	 * */
	public boolean isAssignable(Class<?> clz) {

		// kesovana hodnota - nemusi sa vytvarat zakazdym znova
		if (clz.isInstance(Boolean.TRUE)) {
			return true;
		}
		return false;
	}

	/**
	 * Vracia Boolean rozparsovany z parametra.
	 * 
	 * @return - vyparsovany BigDecimal
	 * */
	public Boolean convertFromString(String string) {
		if (FALSE_LIST.contains(string)) {
			return Boolean.FALSE;
		}

		if (TRUE_LIST.contains(string)) {
			return Boolean.TRUE;
		}

		throw new ImpossibleConversionException("Could not convert value\"" + string + "\" to Boolean.");
	}
}
