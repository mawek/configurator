package cz.mawek.java.skola.dpp.configurator.parser;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import cz.mawek.java.skola.dpp.configurator.Comment;
import cz.mawek.java.skola.dpp.configurator.parser.error.ParserException;

/**
 * @author: Marek Gerhart 3.5.2011
 */

public class CommentParser implements TokenParser {

	private static final Pattern COMMENT_PATTERN = Pattern.compile("^;(.+)$");

	@Override
	public Pattern getTokenPattern() {
		return COMMENT_PATTERN;
	}

	@Override
	public Comment parseToken(String line) {
		Matcher matcher = COMMENT_PATTERN.matcher(line);
		if (!matcher.matches()) {
			throw new ParserException("Line '" + line + "' does't match comment regular expression '" + COMMENT_PATTERN.pattern() + "'");
		}

		String sectionComment = matcher.group(1);
		sectionComment = sectionComment == null ? null : sectionComment.trim();

		return new Comment(sectionComment, line);
	}
}
