package cz.mawek.java.skola.dpp.configurator.parser;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import cz.mawek.java.skola.dpp.Tools;
import cz.mawek.java.skola.dpp.configurator.Configuration;
import cz.mawek.java.skola.dpp.configurator.ConfigurationDefault;
import cz.mawek.java.skola.dpp.configurator.Identifier;
import cz.mawek.java.skola.dpp.configurator.Section;
import cz.mawek.java.skola.dpp.configurator.Token;
import cz.mawek.java.skola.dpp.configurator.parser.error.FileNotFoundUncheckedException;
import cz.mawek.java.skola.dpp.configurator.parser.error.IOUncheckedException;
import cz.mawek.java.skola.dpp.configurator.parser.error.ParserException;

/**
 * @author: Marek Gerhart 30.4.2011
 */

public class IniParser implements ConfigurationParser {

	private static final String INI_FILE_EXTENSION = ".ini";

	@Override
	public boolean canHandle(String fileName) {
		return (!Tools.isEmptyString(fileName) && fileName.toLowerCase().endsWith(INI_FILE_EXTENSION));
	}

	@Override
	public Configuration readConfiguration(String configurationPath) {

		File configurationFile = new File(configurationPath);
		if (!configurationFile.exists()) {
			throw new FileNotFoundUncheckedException("File " + configurationPath + " not found!");
		}

		try {
			return parseConfiguration(configurationFile);
		} catch (FileNotFoundException e) {
			throw new FileNotFoundUncheckedException(e);
		} catch (IOException e) {
			throw new IOUncheckedException(e);
		}
	}

	@Override
	public Configuration reloadConfiguration() {
		// TODO: implement
		return null;
	}

	@Override
	public void saveConfiguration() {
		// TODO: implement

	}

	private ConfigurationDefault parseConfiguration(File configurationFile) throws IOException {
		ConfigurationDefault config = new ConfigurationDefault();

		// add default section
		Section actualSection = null;

		BufferedReader bufRead = new BufferedReader(new FileReader(configurationFile));

		Token token;
		String line = bufRead.readLine();
		int count = 1;

		try {
			while (line != null) {
				token = processConfigLine(line.trim());

				switch (token.getType()) {
				case SECTION:
					actualSection = token.<Section> cast();
					config.addSection(actualSection);
					// actualSection = new Section().getClass().cast(token);
					break;
				case IDENTIFIER:
					if (actualSection == null) {
						throw new ParserException("Identifier outside section. Each identifier must by positioned inside section.");
					}
					config.addParameterValue(actualSection, token.<Identifier> cast());
					break;
				case COMMENT:
					// skip
					break;
				case BLANK_LINE:
					// skip
					break;
				}

				line = bufRead.readLine();
				count++;
			}
		} catch (Exception e) {
			throw new ParserException("Error during parsing configuration. Error line: " + count, e);
		}

		bufRead.close();

		return config;
	}

	private Token processConfigLine(String line) {
		for (TokenType tokenType : TokenType.values()) {
			TokenParser tokenParser = tokenType.getTokenParser();
			if (tokenParser.getTokenPattern().matcher(line).matches()) {
				return tokenParser.parseToken(line);
			}
		}

		throw new ParserException("Line '" + line + "' doesn't match any regular expression.");
		// check ini format?
	}

}
