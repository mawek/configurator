package cz.mawek.java.skola.dpp.configurator.parser.error;

/**
 * Wrapper pre IOExcepsnu, ktora je zbytocne checked.
 * 
 * @author: Marek Gerhart 30.4.2011
 */
public class FileNotFoundUncheckedException extends RuntimeException {
	private static final long serialVersionUID = 1203518891449480564L;

	public FileNotFoundUncheckedException(String message, Throwable cause) {
		super(message, cause);
	}

	public FileNotFoundUncheckedException(String message) {
		super(message);
	}

	public FileNotFoundUncheckedException(Throwable cause) {
		super(cause);
	}
}
