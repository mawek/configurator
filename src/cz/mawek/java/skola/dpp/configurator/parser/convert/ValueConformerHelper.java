package cz.mawek.java.skola.dpp.configurator.parser.convert;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import cz.mawek.java.skola.dpp.configurator.parser.error.ImpossibleConversionException;

/**
 * Trieda sluziaca na zhromazdovanie konvertorov, ktore konvertuju retazcovu hodnotu na pozadovany typ.
 * 
 * @see Conformer
 * 
 * @author: Marek Gerhart 3.5.2011
 */

public final class ValueConformerHelper {
	private ValueConformerHelper() {

	}

	// !zalezi na poradi!
	private static final List<Conformer<?>> conformers = new LinkedList<Conformer<?>>();
	static {
		conformers.add(new IntegerConformer());
		conformers.add(new DoubleConformer());
		conformers.add(new BigDecimalConformer());
		conformers.add(new BooleanConformer());
		conformers.add(new StringConformer());
	}

	/**
	 * Vracia nemodifikovatelny zoznam registrovanych conformerov
	 * */
	public static List<Conformer<?>> getRegisteredConformers() {
		return Collections.unmodifiableList(conformers);
	}

	/**
	 * Zaregistrovat novy conformer. V conformeroch zalezi na pozicii v zozname, pretoze prvy vyhovujuci conformer sa pouzije. Ak nejaku konverziu moze spravit
	 * viacero conformerov v zozname tak sa pouzije prvy vyhovujuci (bezohladu na to ze druhy by to mozno spravil lepsie/efektivnejsie a pod)
	 * 
	 * @param conformer
	 *            - conformer k zaregistrovaniu
	 * 
	 * @param position
	 *            - pozicia na ktoru sa ma conformer v zozname vlozit
	 * @throws IndexOutOfBoundsException
	 *             if position is out of range
	 * */
	public static <T> void registerConformer(Conformer<T> conformer, int position) {
		conformers.add(position, conformer);
	}

	public static <T> void unRegisterConformer(Conformer<T> conformer) {
		conformers.remove(conformer);
	}

	/**
	 * Konverzia retazcovej hodnoty na pozadovany typ. Konverziu robia zaregistrovane conformery - v pripade ze existuje viacej vyhovujucich conformerov pouzije
	 * sa vzdy prvy vyhovujuci zo zoznamu conformerov
	 * 
	 * @param clz
	 *            - typ navratovej hodnoty
	 * @param identifierValue
	 *            - retazcova hodnota ktora sa ma skonvertovat
	 * 
	 * @return hodnota skonvertovana na pozadovany typ
	 * 
	 * @throws ImpossibleConversionException
	 *             ak je identifierValue null alebo aak sa nepodari skonvertovat hodnotu na pozadovany typ alebo ak sa nenajde ziadny vhodny conformer na
	 *             skonvertovanie
	 * */
	@SuppressWarnings("unchecked")
	public static <T> T convert(Class<T> clz, String identifierValue) throws ImpossibleConversionException {
		if (identifierValue == null) {
			throw new ImpossibleConversionException("Cannot convert null to any type.");
		}

		for (Conformer<?> conformer : conformers) {
			if (conformer.isAssignable(clz)) {
				return ((Conformer<T>) conformer).convertFromString(identifierValue);
			}
		}

		throw new ImpossibleConversionException("No conformer found to perform conversion from value[" + identifierValue + "] to class[" + clz.getName() + "].");
	}
}
