package cz.mawek.java.skola.dpp.configurator.parser.convert;

/**
 * Conformer pre typ String - sluzi na konverziu retazcovej reprezentacie String.
 * 
 * @author: Marek Gerhart 3.5.2011
 */

public final class StringConformer implements Conformer<String> {

	/**
	 * @return true ak je parameter typu String
	 * @return false inak
	 * */
	public boolean isAssignable(Class<?> clz) {
		if (clz.isInstance("")) {
			return true;
		}
		return false;
	}

	/**
	 * Vracia retazcovu reprezentaciu parametru
	 * 
	 * @return - retazec
	 * */
	public String convertFromString(String string) {
		return string;
	}
}
