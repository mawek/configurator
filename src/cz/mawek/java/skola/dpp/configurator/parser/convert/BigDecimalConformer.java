package cz.mawek.java.skola.dpp.configurator.parser.convert;

import java.math.BigDecimal;

import cz.mawek.java.skola.dpp.configurator.parser.error.ImpossibleConversionException;

/**
 * Conformer pre typ BigDecimal - sluzi na konverziu retazcovej reprezentacie BigDecimalu.
 * 
 * @author: Marek Gerhart 3.5.2011
 */

public final class BigDecimalConformer implements Conformer<BigDecimal> {

	/**
	 * @return true ak je parameter typu BigDecimal
	 * @return false inak
	 * */
	public boolean isAssignable(Class<?> clz) {

		// kesovana hodnota - nemusi sa vytvarat zakazdym znova
		if (clz.isInstance(BigDecimal.ZERO)) {
			return true;
		}
		return false;
	}

	/**
	 * Vracia BigDecimal rozparsovany z parametra.
	 * 
	 * @return - vyparsovany BigDecimal
	 * */
	public BigDecimal convertFromString(String string) {
		try {
			return new BigDecimal(string);
		} catch (NumberFormatException e) {
			throw new ImpossibleConversionException("Could not convert value\"" + string + "\" to BigDecimal.", e);
		}
	}
}
