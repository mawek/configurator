package cz.mawek.java.skola.dpp.configurator.parser.error;

/**
 * Wrapper pre IOExcepsnu, ktora je zbytocne checked.
 * 
 * @author: Marek Gerhart 30.4.2011
 */
public class IOUncheckedException extends RuntimeException {
	private static final long serialVersionUID = 1203518891449480564L;

	public IOUncheckedException(String message, Throwable cause) {
		super(message, cause);
	}

	public IOUncheckedException(String message) {
		super(message);
	}

	public IOUncheckedException(Throwable cause) {
		super(cause);
	}
}
