package cz.mawek.java.skola.dpp.configurator.parser.error;

/**
 * @author: Marek Gerhart 3.5.2011
 */

public class ImpossibleConversionException extends RuntimeException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8760030829385680791L;

	public ImpossibleConversionException(String message, Throwable cause) {
		super(message, cause);
	}

	public ImpossibleConversionException(String message) {
		super(message);
	}
}
