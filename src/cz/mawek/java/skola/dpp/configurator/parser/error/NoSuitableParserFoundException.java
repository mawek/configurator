package cz.mawek.java.skola.dpp.configurator.parser.error;

/**
 * 
 * 
 * @author: Marek Gerhart 30.4.2011
 */
public class NoSuitableParserFoundException extends RuntimeException {
	private static final long serialVersionUID = 1203518891449480564L;

	public NoSuitableParserFoundException(String message) {
		super(message);
	}

}
