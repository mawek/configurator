package cz.mawek.java.skola.dpp.configurator.parser.error;

/**
 * @author: Marek Gerhart 3.5.2011
 */

public class ParserException extends RuntimeException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8760030829385680791L;

	public ParserException(String message, Throwable cause) {
		super(message, cause);
	}

	public ParserException(String message) {
		super(message);
	}
}
