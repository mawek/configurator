package cz.mawek.java.skola.dpp.configurator;

import cz.mawek.java.skola.dpp.configurator.parser.TokenType;

/**
 * Token reprezentujuci samostatny komentar na riadku.
 * 
 * @author: Marek Gerhart 30.4.2011
 */
public class Comment extends Token {

	public Comment(String comment, String line) {
		this.comment = comment;
		this.textLine = line;
	}

	@Override
	public TokenType getType() {
		return TokenType.COMMENT;
	}
}
