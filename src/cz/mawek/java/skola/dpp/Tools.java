package cz.mawek.java.skola.dpp;

/**
 * Trieda, ktora sluzi na zdruzenie obecnych funkcii.
 * 
 * @author: Marek Gerhart 30.4.2011
 */

public final class Tools {
	private Tools() {
	}

	/**
	 * Vracia priznak ci je retazec prazdny alebo null.
	 * 
	 * @param str
	 *            - skumany retazec
	 * 
	 * @return false ak je string null alebo ak obsahuje len biele znaky
	 * @return true ak sa jedna o neprazdny nenullovy retazec
	 * */
	public static final boolean isEmptyString(String str) {
		if (str == null || "".equals(str.trim())) {
			return true;
		}

		return false;
	}

	/**
	 * Vracia priznak ci sa dva nenullove retazce rovnaju
	 * 
	 * @param one
	 *            , two - porovnavane retazce
	 * 
	 * @return false ak sa retazce nerovnaju alebo ak je jeden z nich null
	 * @return true ak sa hodnoty retazcov rovnaju
	 * */
	public static final boolean isSafeStringEqual(String one, String two) {
		return ((one != null) && (two != null)) && one.equals(two);
	}
}
