package cz.mawek.java.skola.dpp;

/**
 * Obycajny kontajner na uchovavanie dvoch suvisiacich hodnot.
 * 
 * @author: Marek Gerhart 3.5.2011
 */

public class Pair<T, E> {
	protected T parameter;
	protected E value;

	public Pair(T parameter, E value) {
		this.parameter = parameter;
		this.value = value;
	}

	public T getParameter() {
		return parameter;
	}

	public E getValue() {
		return value;
	}

	@Override
	public String toString() {
		return "Parameter:[" + parameter + "], value:[" + value + "]";
	}

}
