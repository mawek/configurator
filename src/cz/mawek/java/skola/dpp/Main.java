package cz.mawek.java.skola.dpp;

import java.util.List;

import cz.mawek.java.skola.dpp.configurator.Configuration;
import cz.mawek.java.skola.dpp.configurator.parser.ConfigurationParserFactory;

/**
 * @author: Marek Gerhart 27.4.2011
 */

public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		String fileName = "D:\\configuration.ini";
		Configuration config = ConfigurationParserFactory.getParserForFileName(fileName).readConfiguration(fileName);

		String str = config.getValue("Sekce 1", "Option 1", String.class);
		String str2 = config.getValue("Sekce 1", "oPtion 1", String.class);

		String strmwk = config.getValue("Sekce 1", "mwk", String.class);

		String str3 = config.getValue("$Sekce::podsekce", "Option 2", String.class);

		Double str4 = config.getValue("Cisla", "float2", Double.class);

		List<Integer> stringList = config.getList("$Sekce::podsekce", "Option 6", Integer.class);

		List<String> stringList2 = config.getList("$Sekce::podsekce", "Option 4", String.class);

		config.changeIdentifierValue("Cisla", "cele", "tralala");
		
		config.writeToFile("D:\\configurationOut.ini");

		System.out.println("ok");
	}

}
